const AWS = require('aws-sdk')
const s3 = new AWS.S3()
const MongoClient = require('mongodb').MongoClient
const dbName = 'fundraiser'
let cachedDb = null
const {
    TERMINAL_ID,
    MID_ID,
    BUCKET_NAME
} = process.env
const connectToDatabase = (uri) => {
    console.log('=> connect to database')
  
    if (cachedDb) {
      console.log('=> using cached database instance')
      return Promise.resolve(cachedDb)
    }
  
    return MongoClient.connect(uri)
      .then((client) => {
        cachedDb = client.db(dbName)
        console.log(cachedDb, 'hello cached db')
        return cachedDb
      })
}

const insertDocument = (db, body) => {
    return new Promise(resolve => {
        db.collection('fundraiser_donators').insert(body).then((res, err) => {
            console.log(res, err)
            resolve()
        })
    })
}

const ashraitInit = (uniq, total, email, name, project, date) => {
    return `<ashrait><request><version>1000</version><language>HEB</language><dateTime/><requestId/><command>doDeal</command><doDeal><email>${email}</email>
<user>${uniq}</user>
<userData1>${name}</userData1>
<userData2>${project}</userData2>
<invoice>
<invoiceCreationMethod>post</invoiceCreationMethod>
<invoiceDate/><invoiceSubject>${name}</invoiceSubject>
<invoiceDiscount/><invoiceDiscountRate/><invoiceItemCode>000</invoiceItemCode>
<invoiceItemDescription>תרומה כנפיים של קרמבו</invoiceItemDescription>
<invoiceItemQuantity>1</invoiceItemQuantity>
<invoiceItemPrice>${total}</invoiceItemPrice>
<invoiceTaxRate/><invoiceComments/>
<companyInfo>${name}</companyInfo>
<mailTo>${email}</mailTo>
<isItemPriceWithTax>0</isItemPriceWithTax>
<ccDate>${date}</ccDate>
<invoiceSignature/>
<invoiceType/></invoice>
<terminalNumber>${TERMINAL_ID}
</terminalNumber>
<cardNo>CGMPI</cardNo>
<total>${total}</total>
<transactionType>Debit</transactionType>
<creditType>Payments</creditType>
<currency>ILS</currency>
<transactionCode>Phone</transactionCode>
<validation>TxnSetup</validation>
<firstPayment></firstPayment>
<periodicalPayment></periodicalPayment>
<numberOfPayments>12</numberOfPayments>
<mid>${MID_ID}</mid>
<uniqueid>${uniq}</uniqueid>
<mpiValidation>AutoComm</mpiValidation>
<successUrl>https://krembo-old-web.krembo.org.il/registration-success</successUrl>
<errorUrl>https://krembo-old-web.krembo.org.il/registration-failed</errorUrl>
<cancelUrl></cancelUrl>
<customerData></customerData>
</doDeal>
</request>
</ashrait>`
}

const now = () => new Date().toISOString()
    

const replaceErrs = string => string.replace(/"/g, "")
.replace(/&/g, "")
.replace(/\t/g, "")
.replace(/\n/g, "")
.replace(/%/g, "")
.replace("$", "")
.replace(/#/g, "")
.replace(/!/g, "")

const uploadS3 = (Body, uniq, success) => {
    return new Promise(resolve => {
        const params = {
            Body,
            ContentType: 'text/html',
            Bucket: BUCKET_NAME,
            Key: `${success}/${uniq}-${Math.floor(+new Date() / 1000)}.txt`
        }
        s3.putObject(params, (err, result) => {
            if(!err) console.log('Successfully written to s3', uniq)
            else console.log('Failed to write to s3', uniq)
            resolve()
        })
    })

}

module.exports = {
    ashraitInit,
    replaceErrs,
    uploadS3,
    connectToDatabase,
    insertDocument,
    now
}