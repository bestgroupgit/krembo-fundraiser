const {
    ashraitInit, 
    replaceErrs, 
    now, 
    uploadS3,
    connectToDatabase,
    insertDocument
} = require('./methods')
const  convert = require('xml-js')
const querystring = require('querystring')
const axios = require('axios')
const {
    MONGODB_URI,
    PASSWORD,
    KREMBO,
    CREDIT_GUARD
} = process.env


exports.handler = async (event) => {
        try {
            if (event.body !== null && event.body !== undefined) {
                let body = JSON.parse(event.body)
                const { name, phone, subscribed } = body
                const email = body.email.toLowerCase()
                const total = body.total * 100
                const uniqueId = parseInt(Math.random() * 100000000)
                if(email && phone && name && total && uniqueId) {
                    const date = now()
                    const ashrait = ashraitInit(
                        uniqueId, 
                        total, 
                        replaceErrs(email).trim(), 
                        replaceErrs(name).trim(), 
                        replaceErrs('משדר כנפיים של קרמבו'), 
                        date.split('T')[0]
                    )
                    const newDocument = {
                        uniqueId,
                        total,
                        email,
                        name,
                        subscribed,
                        date
                    }
                    connectToDatabase(MONGODB_URI)
                    .then(db => insertDocument(db, newDocument))
                    const requestBody = querystring.stringify({
                        user: KREMBO,
                        password: PASSWORD,
                        int_in: ashrait
                    })
                    const config = {
                        headers: {
                          'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    }
                    const response = await axios.post(CREDIT_GUARD, requestBody, config)
                    const request = convert.xml2js(response.data, { compact: true, spaces: 4 })
                    console.log(JSON.stringify(request), 'request')
                    const did_succeed = request.ashrait.response.message._text
                    console.log(did_succeed)
                    if(did_succeed === 'עסקה תקינה') {
                        await uploadS3(JSON.stringify(body), uniqueId, 'success')
                        return {
                            statusCode: 200,
                            body: JSON.stringify(request.ashrait.response.doDeal.mpiHostedPageUrl._text),
                        }
                    } else {
                        await uploadS3(JSON.stringify(body), uniqueId, 'failed')
                    }
                } else {
                    await uploadS3(JSON.stringify(body), uniqueId, 'failed')
                }
            }
            return {
                statusCode: 200,
                body: JSON.stringify('NOT OK'),
            }
        } catch(err) {
            if (event.body !== null && event.body !== undefined) {
                let body = JSON.parse(event.body)
                await uploadS3(JSON.stringify(body), 0000000000, 'failed')
                console.log(err, '**PROCESS FAILED**')
                return {
                    statusCode: 200,
                    body: JSON.stringify('NOT OK'),
                }
            }
        }


}
